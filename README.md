# Sarcasm
This is a simple utility to convert text to aLtErNaTiNg CaPs (or random caps,
or all caps). It is in the spirit of [suckless software](https://suckless.org).

## Uses
This can be very useful when trying to mock someone or be sacrastic on the
internet, as alternating caps are a pain to type. It is best used in a script.
A one-line script using [dmenu](https://tools.suckless.org/dmenu) might look
like this:
```bash
#!/bin/bash

read -r var < <(echo "dmenu -p 'Sarcasm:'" | sh) ; [[ -n "$var" ]] && echo -n $var | sarcasm | xclip -selection clipboard
```

## Installation
Clone this repo, and then simply run `make install` (as root).
To compile with debug info (but not install), run `make devel`.

## Usage
By default, sarcasm will try to read text from the arguments list. If there are
no arguments, it will read from stdin. It prints output to stdout. If you are
familiar with [figlet](http://www.figlet.org/), it behaves similarly.

Thus, you can use it in either of the following ways:
```
sarcasm [options] your text here

echo "your text here" | sarcasm [options]
```
The `-r` option indicates random caps, and the `-c` option indicates all caps.
The `-a` option means alternating caps, which is the default. If you pass
multiple options, the last one will be used.

## License
Licensed under the Apache License, Version 2.0

See LICENSE

## Contributing
1. This is a pretty simple utility, and I doubt anyone will actually use it,
but if you do use it and you find a problem, feel free to open an issue.

2. If you know how to fix it, feel free to save me some work by opening a pull
request. If you do, please
[write a good commit message](https://chris.beams.io/posts/git-commit/), and
use style consistent with the project.
