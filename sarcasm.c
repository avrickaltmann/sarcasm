/*
 * Copyright 2021 AJ Williams
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *
 */

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <time.h>

#define SIZ 20

void altCaps(char *str);
void randCaps(char *str);
void allCaps(char *str);

int main(int argc, char *argv[])
{
    /* mode:
     * 0 = default (alternating caps)
     * 1 = all caps
     * -1 = random caps
     */
    int mode = 0;
    int c;

    // parse options
    while ((c = getopt(argc, argv, "arc")) != -1)
    {
        switch (c)
        {
            case 'a':
                mode = 0;
                break;
            case 'r':
                mode = -1;
                break;
            case 'c':
                mode = 1;
                break;
            default:
                printf("error: unrecognized option\n");
                abort();
        }
    }

    char *str = NULL;

    // read from non-option args, if any
    int allocd = 1;
    for (int i = optind; i < argc; i++)
    {
        str = realloc(str, (allocd += strlen(argv[i]) + 1));
        strcat(str, argv[i]);
        if (i != argc - 1)
            strcat(str, " ");
    }

    // if not, read stdin
    if (str == NULL)
    {
        char buf[SIZ];
        int bufs = 0;
        size_t p;

        while (p = fread(buf, 1, SIZ, stdin))
        {
            str = realloc(str, bufs * sizeof(buf) + p + 1);
            memcpy(str + bufs * sizeof(buf), buf, p);
            bufs++;
        }
    }

    // alternate caps
    bool caps = false;
    char ltr;
    switch (mode)
    {
        case 0:
            altCaps(str);
            break;
        case -1:
            randCaps(str);
            break;
        case 1:
            allCaps(str);
            break;
        default:
            printf("internal error\n");
            break;
    }

    printf("%s", str);
    free(str);
}

void altCaps(char *str)
{
    bool caps = false;
    for (int i = 0, n = strlen(str); i < n; i++)
    {
        if (isalpha(str[i]))
        {
            if (caps)
                str[i] = toupper(str[i]);
            else
                str[i] = tolower(str[i]);
            caps = !caps;
        }
    }
}

void randCaps(char *str)
{
    srandom(time(0));

    bool caps = random() % 2;
    for (int i = 0, n = strlen(str); i < n; i++)
    {
        if (isalpha(str[i]))
        {
            if (caps)
                str[i] = toupper(str[i]);
            else
                str[i] = tolower(str[i]);
            caps = random() % 2;
        }
    }
}

void allCaps(char *str)
{
    for (int i = 0, n = strlen(str); i < n; i++)
    {
        if (isalpha(str[i]))
            str[i] = toupper(str[i]);
    }
}
